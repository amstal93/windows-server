
# "timestamp" template function replacement
locals { timestamp = regex_replace(timestamp(), "[- TZ:]", "") }

source "vsphere-iso" "windows" {
  host                 = "${var.vcenter_host}"
  datacenter           = "${var.vcenter_dc}"
  datastore            = "${var.vcenter_datastore}"
  convert_to_template  = true
  folder               = "${var.vcenter_folder}"
  guest_os_type        = "${var.guest_os_type}"
  shutdown_command     = "shutdown /s /t 10 /f /d p:4:1 /c \"Packer Shutdown\""
  boot_order           = "disk,cdrom"
  boot_wait            = "5s"
  iso_paths            = ["${var.os_iso_path}"]
  CPUs                 = "${var.cpus}"
  RAM                  = "${var.ram}"
  network_adapters {
    network      = "${var.network}"
    network_card = "vmxnet3"
  }
  disk_controller_type = ["pvscsi"]
  storage {
    disk_size             = "${var.disk_size}"
    disk_thin_provisioned = "${var.disk_thin_provisioned}"
  }
  floppy_files          = [
    "./answer_files/${var.os_version}/Autounattend.xml",
    "./scripts/_common/Enable-Winrm.ps1",
    "./scripts/_common/Install-VMWareTools.ps1",
    "./drivers/"
  ]
  # Conection
  communicator          = "winrm"
  winrm_username        = var.winrm_username
  winrm_password        = var.winrm_password
  winrm_timeout         = "12h"
  winrm_port            = "5985"
  # vSphere
  vcenter_server = "${var.vcenter_address}"
  insecure_connection  = "${var.vcenter_ignore_ssl}"
  username       = "${var.vcenter_user}"
  password               = "${var.vcenter_password}"
  vm_name        = "${ var.os_family }-${ var.os_version }-{{ isotime \"2006-01-02\" }}"
}

build {
  sources = ["source.vsphere-iso.windows"]

<<<<<<< HEAD
  # provisioner "powershell" {
  #   elevated_user = var.winrm_username
  #   elevated_password = var.winrm_password
  #   scripts = [
  #     "scripts/_common/Disable-UAC.ps1"
  #   ]
  # }
  # provisioner "windows-update" {
  #   pause_before = "30s"
  #   filters = [
  #     "exclude:$_.Title -like '*Preview*'",
  #     "include:$true"
  #   ]
  # }
  # provisioner "powershell" {
  #   elevated_user = var.winrm_username
  #   elevated_password = var.winrm_password
  #   scripts = [
  #     "scripts/_common/Remove-UpdateCache.ps1",
  #     "scripts/_common/Invoke-Defrag.ps1",
  #     "scripts/_common/Reset-EmptySpace.ps1"
  #   ]
  # }

  # provisioner "windows-restart" {
  #   restart_timeout = "1h"
  # }

  provisioner "ansible" {
    pause_before = "30s"
    playbook_file = "./ansible/playbook.yml"
    user = "vagrant"
    use_proxy = false
    ansible_env_vars = ["WINRM_PASSWORD=${var.winrm_password}"]
=======
  provisioner "powershell" {
    elevated_user = var.winrm_username
    elevated_password = var.winrm_password
    scripts = [
      "scripts/_common/Disable-UAC.ps1"
    ]
  }
  
  provisioner "windows-update" {
    pause_before = "30s"
    filters = [
      "exclude:$_.Title -like '*VMware*'",
      "exclude:$_.Title -like '*Preview*'",
      "include:$true"
    ]
  }

  provisioner "windows-restart" {
    restart_timeout = "1h"
  }

  provisioner "ansible" {
    pause_before = "30s"
    playbook_file = "./ansible/playbook.yml"
    user = "vagrant"
    use_proxy = false
    ansible_env_vars = ["WINRM_PASSWORD=${var.winrm_password}"]
  }

  provisioner "windows-restart" {
    restart_timeout = "1h"
  }

  provisioner "powershell" {
    pause_before = "30s"
    elevated_user = var.winrm_username
    elevated_password = var.winrm_password
    scripts = [
      "scripts/_common/Save-Shutdown.ps1"
    ]
>>>>>>> 60ef8ecbd352ef100600a2b5db1e38d8833a79b1
  }

}
