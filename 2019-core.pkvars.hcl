# OS Info
os_version      = "2019-core"
os_family       = "windows-server"
os_iso_path      = "[helios-nfs] ISO/windows_server_2019.ISO"
guest_os_type   = "windows9Server64Guest"

# VM
cpus      = 4
ram       = 4096
disk_size = 32768
network   = "vlan21"

# Build
winrm_username   = "vagrant"
winrm_password   = "vagrant"

# VMware
vcenter_host      = "helios-esx-1.hardac.net"